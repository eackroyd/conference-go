from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_picture_url(query):
    url = f"https://api.pexels.com/v1/search/?query={query}"

    header = {
        "Authorization": PEXELS_API_KEY,
    }

    response = requests.get(url, headers=header)
    api_dict = response.json()
    return api_dict["photos"][0]["src"]["original"]


def get_weather_data(city, state):

    geocode_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"

    location_info = requests.get(geocode_url)
    l_dict = location_info.json()
    lat, lon = l_dict[0]["lat"], l_dict[0]["lon"]

    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    api_dict = response.json()
    description = api_dict["weather"][0]["description"]
    temp = api_dict["main"]["temp"]

    return {
        "temp": temp,
        "description": description,
    }
